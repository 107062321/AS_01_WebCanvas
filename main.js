var color_now = 'black';
var baldness = 1;
var step = -1;
var history = [];
var count = -1;
var eraser = false;
var startx;
var starty;
var endx;
var endy;
var circle = false;
var square = false;
var triangle = false;
var line = false;
var fill = false
var texting = false;
var pic;
var text_number = 0;
var t_posx;
var t_posy;
var font_f = 'Arial';
var font_s = 16;
var rainbow = false;
var textbox = document.getElementById('textbox');
var cnt = 0;
var tmpx;
var tmpy;




window.onload = function(){
    var canvas = document.querySelector('#myCanvas');
    push();
    document.getElementById("myCanvas").style.cursor = 'url("./img/pen.png") 0 16, auto';
    canvas.addEventListener('mousedown',mouseDown);
    canvas.addEventListener('mousemove',mouseMove);
    canvas.addEventListener('mouseup',mouseUp);
    canvas.addEventListener('mouseout',mouseOut);
}



function mouseDown(e){
    pic = new Image();
    pic.src = history[step];
    this.draw = true;
    this.ctx = this.getContext("2d");
    this.ctx.strokeStyle= color_now;
    this.ctx.lineWidth= baldness;
    this.ctx.lineJoin = "round";
    this.ctx.lineCap = "round";

    var o = this;
    this.offsetX = this.offsetLeft;
    this.offsetY = this.offsetTop;

    while(o.offsetParent){
    	o = o.offsetParent;
    	this.offsetX += o.offsetLeft;
    	this.offsetY += o.offsetTop;
    }


    this.ctx.beginPath();
    this.ctx.moveTo(e.pageX - this.offsetX, e.pageY - this.offsetY);
    startx = e.pageX-this.offsetX;
    starty = e.pageY-this.offsetY;
    //console.log(startx, starty);
    if(texting === true && text_number === 0) {
        drawtext(e);
        t_posx = startx;
        t_posy = starty;
    }
    console.log(e.pageX);
}

function mouseMove(e){
    
    var canvas = document.querySelector('#myCanvas');
    var ctx = canvas.getContext("2d");
    if (this.draw){
        if(eraser){
            this.ctx.globalCompositeOperation = "destination-out";
            this.ctx.lineTo(e.pageX-this.offsetX,e.pageY-this.offsetY);
            this.ctx.stroke();
        }
        else if(circle || square || triangle || line){
            this.ctx.globalCompositeOperation = "source-over";
            endx = e.pageX-this.offsetX;
            endy = e.pageY-this.offsetY;
            if(circle === true){
                var pic = new Image();
                pic.src = history[step];
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(pic, 0, 0);
                // pic.onload = function() {
                    
                // }
                var radius = Math.sqrt(Math.pow((endx-startx), 2) + Math.pow((endy-starty), 2))/2;
                this.ctx.beginPath();
                this.ctx.arc((endx+startx)/2, (endy+starty)/2, radius, 0, 2*Math.PI);
                this.ctx.fillStyle = color_now;
                if(fill)this.ctx.fill();
                this.ctx.stroke();
                this.ctx.closePath();
            }
            else if(square === true){
                var pic = new Image();
                pic.src = history[step];
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(pic, 0, 0);
                // pic.onload = function() {
                    
                // }
                this.ctx.beginPath();
                this.ctx.fillStyle = color_now;
                if(fill)this.ctx.fillRect(startx, starty, endx-startx, endy-starty);
                else this.ctx.strokeRect(startx, starty, endx-startx, endy-starty)
                this.ctx.closePath();
            }
            else if(triangle === true){
                var pic = new Image();
                pic.src = history[step];
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(pic, 0, 0);
                // pic.onload = function() {
                    
                // }
                this.ctx.beginPath();
                this.fillStyle = color_now;
                this.ctx.moveTo((startx + endx)/2, starty);
                this.ctx.lineTo(endx, endy);
                this.ctx.lineTo(startx, endy);
                this.ctx.closePath();
                if(fill)this.ctx.fill();
                this.ctx.stroke();
            }
            else if(line === true){
                var pic = new Image();
                pic.src = history[step];
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(pic, 0, 0);
                // pic.onload = function() {
                //     ctx.clearRect(0, 0, canvas.width, canvas.height);
                //     ctx.drawImage(pic, 0, 0);
                // }
                this.ctx.beginPath();
                this.fillStyle = color_now;
                this.ctx.moveTo(startx, starty);
                this.ctx.lineTo(endx, endy);
                this.ctx.stroke();
            }
        }
        else if(texting){
            ;
        }
        else if(rainbow){
            this.ctx.beginPath();
            this.ctx.globalCompositeOperation = "source-over";
            this.ctx.strokeStyle = "hsl(" + cnt + ", 100%, 50%)";
            cnt= cnt+2;
            this.ctx.moveTo(tmpx, tmpy);
            this.ctx.lineTo(e.pageX-this.offsetX,e.pageY-this.offsetY);
            this.ctx.stroke();
        }
        else{
            this.ctx.globalCompositeOperation = "source-over";
            this.ctx.lineTo(e.pageX-this.offsetX,e.pageY-this.offsetY);
            this.ctx.stroke();
        }
    }
    tmpx = e.pageX-this.offsetX;
    tmpy = e.pageY-this.offsetY;
}

function mouseUp(e){
    this.draw = false;
    endx = e.pageX-this.offsetX;
    endy = e.pageY-this.offsetY;
    if(circle === true){
        var radius = Math.sqrt(Math.pow((endx-startx), 2) + Math.pow((endy-starty), 2))/2;
        this.ctx.beginPath();
        this.ctx.arc((endx+startx)/2, (endy+starty)/2, radius, 0, 2*Math.PI);
        this.ctx.fillStyle = color_now;
        if(fill)this.ctx.fill();
        this.ctx.stroke();
        this.ctx.closePath();
    }
    else if(square === true){
        this.ctx.beginPath();
        this.ctx.fillStyle = color_now;
        if(fill)this.ctx.fillRect(startx, starty, endx-startx, endy-starty);
        else this.ctx.strokeRect(startx, starty, endx-startx, endy-starty)
        this.ctx.closePath();
    }
    else if(triangle === true){
        this.ctx.beginPath();
        this.fillStyle = color_now;
        this.ctx.moveTo((startx + endx)/2, starty);
        this.ctx.lineTo(endx, endy);
        this.ctx.lineTo(startx, endy);
        this.ctx.closePath();
        if(fill)this.ctx.fill();
        this.ctx.stroke();
    }
    else if(line === true){
        this.ctx.beginPath();
        this.fillStyle = color_now;
        this.ctx.moveTo(startx, starty);
        this.ctx.lineTo(endx, endy);
        this.ctx.stroke();
    }
    if(!texting)push();
}

function mouseOut(e){
    this.draw = false;
    // console.log("mouseout");
    // push();
}

function erase(){
    eraser = true;
    circle = false;
    square = false;
    triangle = false;
    line = false;
    texting = false;
    rainbow = false;
    document.getElementById("myCanvas").style.cursor = 'url("./img/eraser.png") 0 30, auto';
}

function changebald() {
    baldness = document.getElementById("pen_bald").value;
}

function clearall() {
    var canvas = document.querySelector('#myCanvas');
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0,0,canvas.width,canvas.height);
    history = [];
    count = -1;
    step = -1;
    push();
}

function save() {
    var canvas = document.querySelector('#myCanvas');
    var dataurl = canvas.toDataURL("img/png");

    var a = document.createElement("a");
    document.body.appendChild(a);
    a.href = dataurl;
    a.download = 'mycanvas.png';
    a.click();
}

var up = document.querySelector("#btn_up");
up.addEventListener('change', upload);

function upload(e){
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    var img = new Image();
    img.onload = function(){
        canvas.width = this.width;
        canvas.height = this.height;
        ctx.drawImage(this, 0, 0);
        push();
    }
    img.src = URL.createObjectURL(this.files[0]);
    this.value = "";
}

function changecolor(){
    color_now = document.getElementById("color_plate").value;
}

function push() {
    //console.log("push!!!!!!!!!");
    var canvas = document.getElementById("myCanvas");
    step++; count++;
    if (step < history.length) {
        history.length = step;
    }
    history[step] = canvas.toDataURL();
}

function undo(){
    var canvas = document.getElementById("myCanvas");
    canvas.ctx.globalCompositeOperation = "source-over";
    if (step > 0) {
        step--; 
        var pic = new Image();
        pic.src = history[step];
        pic.onload = function() {
            canvas.ctx.clearRect(0, 0, canvas.width, canvas.height);
            canvas.ctx.drawImage(pic, 0, 0);
        }
    }
    //console.log("step: ", step);
}

function redo(){
    //console.log("step: ", step, ' history.length: ', history.length, ' count:', count);
    var canvas = document.getElementById("myCanvas");
    if (step < count) {
        step++;
        var pic = new Image();
        pic.src = history[step];
        pic.onload = function() {
            canvas.ctx.clearRect(0, 0, canvas.width, canvas.height);
            canvas.ctx.drawImage(pic, 0, 0);
        }
    }
}

function pencil(){
    circle = false;
    square = false;
    triangle = false;
    line = false;
    eraser = false;
    texting = false;
    rainbow = false;
    document.getElementById("myCanvas").style.cursor = 'url("./img/pen.png") 0 30, auto';
}

function drawcircle() {
    circle = true;
    square = false;
    triangle = false;
    line = false;
    eraser = false;
    texting = false;
    rainbow = false;
    document.getElementById("myCanvas").style.cursor = 'url("./img/circle.png") 0 30, auto';
}

function drawsquare() {
    circle = false;
    square = true;
    triangle = false;
    line = false;
    eraser = false;
    texting = false;
    rainbow = false;
    document.getElementById("myCanvas").style.cursor = 'url("./img/square.png") 0 30, auto';
}

function drawtriangle() {
    circle = false;
    square = false;
    triangle = true;
    line = false;
    eraser = false;
    texting = false;
    rainbow = false;
    document.getElementById("myCanvas").style.cursor = 'url("./img/triangle.png") 0 30, auto';
}

function drawline() {
    circle = false;
    square = false;
    triangle = false;
    line = true;
    eraser = false;
    texting = false;
    rainbow = false;
    document.getElementById("myCanvas").style.cursor = 'url("./img/line.png") 0 30, auto';
}

function dofill(){
    fill = true;
}

function dounfill(){
    fill = false;
}

function text(){
    circle = false;
    square = false;
    triangle = false;
    line = false;
    eraser = false;
    texting = true;  
    rainbow = false;
    document.getElementById("myCanvas").style.cursor = 'text';
}



function drawtext(e){
    textbox.style.left = e.pageX + "px";
    textbox.style.top = e.pageY + "px";
    text_number = 1;
    console.log("visible");
    textbox.style.visibility = "visible";
}

textbox.addEventListener("change", function(){
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");
    ctx.globalCompositeOperation = "source-over";
    text_number = 0;
    var inputtext = textbox.value;
    //console.log(inputtext);
    ctx.fillStyle = color_now;
    ctx.font = font_s + 'px ' + font_f;

    ctx.fillText(inputtext, t_posx+2, t_posy+15);
    textbox.style.visibility = "hidden";
    textbox.value = "";
    push();
})

function change_fontfam(){
    
    var tmp = document.getElementById("font_chosen");
    font_f = tmp.value;
    //console.log(font_f);
}

function change_fontsize(){
    
    var tmp = document.getElementById("font_size");
    font_s = tmp.value;
    //console.log(font_s);
}

function drawrainbow(){
    circle = false;
    square = false;
    triangle = false;
    line = false;
    eraser = false;
    texting = false;
    rainbow = true;
    cnt = 0;
    document.getElementById("myCanvas").style.cursor = 'url("./img/pen.png") 0 16, auto';
}