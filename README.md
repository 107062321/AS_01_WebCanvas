# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| 1. Rainbow pen  2. Fill and Unfill the geometrical shape                             | 1~5%     | Y         |


---

### How to use 

![](https://i.imgur.com/cMo0Iwx.png)

        首先看到是我的第一排工具列，他由左至右的功能是：
        左一：他是用來調整筆刷、橡皮擦、圖形的邊框的粗細。
        左二：讓使用者選擇輸入的字型。
        左三：讓使用者選作畫時的顏色，無論是筆刷或圖形的顏色。
        左四：讓使用者選擇輸入的字體大小。
![](https://i.imgur.com/0HM51X9.png)


        再來看到第二排工具列，他由左至右的功能是：
        左一：他是一進入此網頁就會自動選取的工具，畫筆！讓使用者在畫布裡自由的繪畫。
        左二：讓使用者可以擦拭在畫布上的痕跡。
        左三：讓使用者可以在畫布上輸入文字。
![](https://i.imgur.com/nEgZ1Nl.png)
        
        接著看到第三排工具列，他由左至右的功能是：
        左一：讓使用者可以將畫布存取下來成為一個png檔。
        左二：讓使用者可以將圖片上傳上畫布。
        左三：讓使用者可以將畫布直接清空。
![](https://i.imgur.com/Gua4IGD.png)

        往下看到第四排工具列，他由左至右的功能是：
        左一：讓使用者可以返回前一步的操作。
        左二：讓使用者可以重做後一步的操作。
        左三：讓使用者可以在畫布上畫一個圓。
![](https://i.imgur.com/7E4BJth.png)

        再來看到第五排工具列，他由左至右的功能是：
        左一：讓使用者可以在畫布上畫正方形。
        左二：讓使用者可以在畫布上畫三角形。
        左三：讓使用者可以在畫布上畫直線。
![](https://i.imgur.com/rxjEdsu.png)


        最後看到第六排工具列，他由左至右的功能是：
        左一：讓使用者可以在將圖形內部填滿。
        左二：讓使用者可以畫出只有外框的圖形，也是預設的選項。
        左三：讓使用者可以用畫筆畫出連續的顏色，像彩虹一樣。


        


### Function description

        首先說明Fill的和Unfill部分，這部分很簡單，只要button被click
        就將fill設成true或false，然後再畫圖形的時候再根據這個變數判斷
        要不要ctx.fill()
![](https://i.imgur.com/TrJReEP.png)

        再來說明rainbow pen的部分，其實他跟正常的畫筆很像，只是他在每
        次移動，顏色都需要被換過。
        所以我就設了一個global的cnt，當如果今天是 rainbow mode的時候
        我的strokeStyle就改成用hsl的色相、飽和度、亮度的上色方式來上色
        我上網查的結果把飽和度和亮度設在100%和50%會是最符合我希望的顏色，
        然後我把色相的地方設成cnt，接著每次mousemove的時候，cnt=cnt+2
        這樣我就能達到我所希望的rainbow pen的功能
![](https://i.imgur.com/Cg7T4A1.png)

    


### Gitlab page link

       https://107062321.gitlab.io/AS_01_WebCanvas
[click me](https://107062321.gitlab.io/AS_01_WebCanvas)

### Others (Optional)

    謝謝助教每次都花很多時間看我們的Lab，然後幫助我們，討論區的問題也回覆得很快。
    推推！！
    
<style>
table th{
    width: 100%;
}
</style>


###### tags: `AS_Web_Canvas`